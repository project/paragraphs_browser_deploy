SUMMARY
------------

This module allows you to deploy images for the paragraphs browser module.

Two Drush commands are available :

paragraphs-browser-deploy:deploy : Update image of paragraphs browser
paragraphs-browser-deploy:change : Update image of paragraphs browser
and change browser image path.

    It is necessary to have an images folder, and the images must have
    the system name of the paragraph.
    It is possible to change the url of the folder.

  This implementation is sponsored by the GAYA web agency.


REQUIREMENTS
------------

  This module requires the module Paragraphs Browser.


INSTALLATION
------------

   * Install as you would normally install a contributed Drupal module.
