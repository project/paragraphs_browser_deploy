<?php

namespace Drupal\paragraphs_browser_deploy\Commands;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\File\FileExists;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\FileRepository;
use Drush\Commands\DrushCommands;

/**
 * Drush Commands for the module Paragraphs Browser Deploy.
 */
class ParagraphsBrowserCommands extends DrushCommands {

  /**
   * The entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The extension path resolver.
   *
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected $extensionPathResolver;

  /**
   * The file repository service.
   *
   * @var \Drupal\file\FileRepository
   */
  protected $fileRepository;

  /**
   * Configuration object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $configDeploy;

  /**
   * Filesystem.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Entity type manager.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * ParagraphsBrowserDeploy constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityTypeBundleInfo
   *   Entity type bundle info.
   * @param \Drupal\Core\Extension\ExtensionPathResolver $extension_path_resolver
   *   Extension Path Resolver.
   * @param \Drupal\file\FileRepository $fileRepository
   *   File Repository.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   Config Factory.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   File System.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type Manager.
   */
  public function __construct(EntityTypeBundleInfoInterface $entityTypeBundleInfo, ExtensionPathResolver $extension_path_resolver, FileRepository $fileRepository, ConfigFactory $config_factory, FileSystemInterface $fileSystem, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct();
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;
    $this->extensionPathResolver = $extension_path_resolver;
    $this->fileRepository = $fileRepository;
    $this->configDeploy = $config_factory->get('paragraphs_browser_deploy.settings');
    $this->fileSystem = $fileSystem;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Update image of paragraphs browser.
   *
   * @command paragraphs-browser-deploy:deploy
   * @usage paragraphs-browser-deploy:deploy
   * @aliases pbdd
   */
  public function deploy(?string $paragraph = NULL) {
    $deploy = $this->deployAll(FALSE, $paragraph);
    if ($deploy) {
      $this->logger()->success(dt("Deploy of images of Paragraphs browser done."));
    }
  }

  /**
   * Update image of paragraphs browser and change browser image path.
   *
   * @command paragraphs-browser-deploy:change
   * @usage paragraphs-browser-deploy:change
   * @aliases pbdc
   */
  public function change(?string $paragraph = NULL) {
    $deploy = $this->deployAll(TRUE, $paragraph);
    if ($deploy) {
      $this->logger()->success(dt("Deploy of images of Paragraphs browser and browser image uri change done."));
    }
  }

  /**
   * Deploy function.
   *
   * @param bool $change
   *   Allow to save path of image into settings paragraph url.
   * @param string|null $paragraph
   *   Update a specific bundle of a paragraph.
   *
   * @return bool
   *   Returns whether the deployment went well or not.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function deployAll(bool $change = FALSE, ?string $paragraph = NULL) {
    $directory = 'public://paragraphs_image';
    $bundles = $this->entityTypeBundleInfo->getBundleInfo('paragraph');
    if ($paragraph and isset($bundles[$paragraph])) {
      $bundles = [$paragraph => $paragraph];
    }
    if ($this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY)) {
      foreach ($bundles as $id => $bundle) {
        $filename = $id . '.png';
        $uri = $directory . '/' . $filename;
        $folder = $this->configDeploy->get('folder_img_origin');
        if (!$folder) {
          $folder = $this->extensionPathResolver->getPath('module', 'paragraphs_browser_deploy') . '/img';
        }
        $img = $folder . '/' . $filename;
        if (file_exists($img)) {
          $img_content = file_get_contents($img);
          if (file_exists($uri)) {
            $img_old = file_get_contents($uri);
            if (md5($img_content) !== md5($img_old)) {
              $this->saveImage($filename, $img_content);
              $this->logger()->notice($id . ' : Image updated');
            }
            else {
              $this->logger()->notice($id . ' : No change');
            }
          }
          else {
            $this->saveImage($filename, $img_content);
            $this->logger()->notice($id . ' : Image added');
          }
        }
        else {
          $this->logger()->notice($id . ' : Image missing');
        }

        if ($change) {
          $paragraphs_type = $this->entityTypeManager->getStorage('paragraphs_type')->load($id);
          if (file_exists($img)) {
            $paragraphs_type->setThirdPartySetting('paragraphs_browser', 'image_path', $uri);
          }
          else {
            $paragraphs_type->unsetThirdPartySetting('paragraphs_browser', 'image_path');
          }
          $paragraphs_type->save();
        }
      }
    }
    else {
      $this->logger()->error(dt("Impossible to create file."));
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Save image.
   *
   * @param string $filename
   *   The filename.
   * @param string $img_content
   *   Image content.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function saveImage(string $filename, string $img_content) {
    $file = $this->fileRepository->writeData($img_content, 'public://paragraphs_image/' . $filename, FileExists::Replace);
    $file->setPermanent();
    $file->save();
  }

}
