<?php

namespace Drupal\paragraphs_browser_deploy\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings Form form Paragraphs Browser Deploy.
 */
class ParagraphsBrowserDeploySettingsForm extends ConfigFormBase {

  /**
   * The extension path resolver.
   *
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected $extensionPathResolver;

  /**
   * Creates a new ParagraphsBrowserDeploySettingsForm.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *   The typed config manager.
   * @param \Drupal\Core\Extension\ExtensionPathResolver $extension_path_resolver
   *   The extension path resolver.
   */
  public function __construct(ConfigFactoryInterface $config_factory, TypedConfigManagerInterface $typedConfigManager, ExtensionPathResolver $extension_path_resolver) {
    parent::__construct($config_factory, $typedConfigManager);
    $this->extensionPathResolver = $extension_path_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('extension.path.resolver'),
    );
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId(): string {
    return 'paragraphs_browser_deploy_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['paragraphs_browser_deploy.settings'];
  }

  /**
   * Defines the settings form for paragraphs browser deploy.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['folder_img_origin'] = [
      '#title' => $this->t('Origin folder images path'),
      '#type' => 'textfield',
      '#default_value' => $this->getSettings('folder_img_origin', $this->extensionPathResolver->getPath('module', 'paragraphs_browser_deploy') . '/img/'),
      '#required' => FALSE,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * Allow to get settings or default value.
   *
   * @param string $name
   *   The name of settings.
   * @param string $default
   *   Give a default value.
   *
   * @return string
   *   The value of the settings or the default value if empty.
   */
  public function getSettings(string $name, string $default = '') {
    $site_config = $this->config('paragraphs_browser_deploy.settings');
    return $site_config->get($name) ?? $default;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('paragraphs_browser_deploy.settings')
      ->set('folder_img_origin', $form_state->getValue('folder_img_origin'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
